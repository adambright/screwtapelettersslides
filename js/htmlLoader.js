function handleXHR(xhr, section) {
  return function () {
            if (xhr.readyState === 4) {
                // file protocol yields status code 0 (useful for local debug, mobile applications etc.)
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status === 0) {

                    section.innerHTML = xhr.responseText;

                } else {

                    section.outerHTML = '<section data-state="alert">' +
                        'ERROR: The attempt to fetch a slide failed with HTTP status ' + xhr.status + '.' +
                        'Check your browser\'s JavaScript console for more details.' +
                        '<p>Remember that you need to serve the presentation HTML from a HTTP server.</p>' +
                        '</section>';

                }
            }
        };
}

function loadHTMLSlides() {
    var sections = document.querySelectorAll('[data-html]');

    for (var i = 0, len = sections.length; i < len; i++) {

        var section = sections[i],
            xhr = new XMLHttpRequest(),
            url = section.getAttribute('data-html');

        xhr.onreadystatechange = handleXHR(xhr, section);

        xhr.open('GET', url, true);
        xhr.send();
    }
}

loadHTMLSlides();
