function runAsync(func) {
    setTimeout(func, 0);
}

function swapArrows() {
    Velocity(document.querySelector("g#straight-arrow"), {translateY: -220}, 1000);
    Velocity(document.querySelector("g#turn-back-arrow"), {translateY: 220}, 1000);
}

function unswapArrows() {
    Velocity(document.querySelector("g#straight-arrow"), {translateY: 0}, 1000);
    Velocity(document.querySelector("g#turn-back-arrow"), {translateY: 0}, 1000);
}

function unsubscribeIntroExtro(e) {
            Reveal.removeEventListener('fragmentshown', swapArrows);
            Reveal.removeEventListener('fragmenthidden', unswapArrows);
            Reveal.removeEventListener("slidechanged", unsubscribeIntroExtro);
}

Reveal.addEventListener("intro-extro", function() {
    Reveal.addEventListener('fragmentshown', swapArrows);
    Reveal.addEventListener('fragmenthidden', unswapArrows);

    runAsync(function() {
        Reveal.addEventListener("slidechanged", unsubscribeIntroExtro);
    });
}, false);