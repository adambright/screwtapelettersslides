var presenterID = "",
    userID = "",
    currentState = {},
    loaded = false;
const controller = new Firebase("https://amber-heat-4480.firebaseio.com"),
    presentationState = "screwtapeLetters/presentationState";

function pushState(e) {
    var state = Reveal.getState();
    state.indexf = state.indexf === undefined ? -1 : state.indexf;
    controller.child(presentationState).update(state);
}

function setAsPresenter() {
    if (userID.trim() == presenterID.trim()) {
        document.getElementById("sync").style.display = "none";
        pushState();

        controller.child(presentationState).off();
        Reveal.removeEventListener('slidechanged', limitSlideProgress);
        Reveal.removeEventListener('fragmentshown', limitFragmentProgress);
        Reveal.removeEventListener('fragmenthidden', limitFragmentProgress);

        Reveal.addEventListener('slidechanged', pushState);
        Reveal.addEventListener('fragmentshown', pushState);
        Reveal.addEventListener('fragmenthidden', pushState);
    }
}

function limitSlideProgress(e) {
    if (e.indexh > currentState.indexh || (e.indexh == currentState.indexh && e.indexv > currentState.indexv)) {
        Reveal.setState(currentState);
    } else if (e.indexh < currentState.indexh || (e.indexh == currentState.indexh && e.indexv < currentState.indexv)) {
        if (loaded)
            document.getElementById("sync").style.display = "block";
    }
}

function limitFragmentProgress(e) {
    var state = Reveal.getState();
    if (state.indexh == currentState.indexh && state.indexv == currentState.indexv && state.indexf > currentState.indexf) {
        Reveal.setState(currentState);
    } else if (state.indexh <= currentState.indexh && state.indexv <= currentState.indexv && state.indexf < currentState.indexf) {
        if (loaded)
            document.getElementById("sync").style.display = "block";
    }
}

function setAsViewer() {
    document.getElementById("sync").style.display = "none";
    loaded = false;

    controller.child(presentationState).on("value", function(snapshot) {
        currentState = snapshot.val();
        if (currentState !== null) {
            if (document.getElementById("sync").style.display === "none" || document.getElementById("sync").style.display === "") {
                Reveal.setState(currentState);

                if (!loaded) {
                    Reveal.addEventListener('slidechanged', limitSlideProgress);
                    Reveal.addEventListener('fragmentshown', limitFragmentProgress);
                    Reveal.addEventListener('fragmenthidden', limitFragmentProgress);
                    loaded = true;
                }
            }
        }
    });
}

function authenticate() {
    controller.authWithOAuthPopup("google", function(error, authData) {
        if (error) {
            console.log("Login Failed!", error);
        } else {
            userID = authData.uid;

            controller.child("users/admin/uid").on("value", function(snapshot) {
                presenterID = snapshot.val();

                if (presenterID == userID) {
                    setAsPresenter()
                }
                controller.child("users/admin/uid").off();
            });
        }
    });
}

setAsViewer();

if (window.location.href.split("?").length > 1 && window.location.href.split("?")[1].split("#")[0] === "role=presenter") {
    authenticate();
}

document.body.onkeyup = function(e) {
    if (e.keyCode === 49) {
        authenticate();
    }
}
